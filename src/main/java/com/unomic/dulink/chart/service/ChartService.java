package com.unomic.dulink.chart.service;

import com.unomic.dulink.chart.domain.ChartVo;


public interface ChartService {
	public String getDvcNameList(ChartVo chartVo) throws Exception;
	public ChartVo getCurrentDvcData(ChartVo chartVo) throws Exception;
	public String getTimeData(ChartVo chartVo) throws Exception;
	public String getDetailBlockData(ChartVo chartVo) throws Exception;
	public String getAlarmList(ChartVo chartVo) throws Exception;
	
	
	
	//common func
	public String getAppList(ChartVo chartVo) throws Exception;
	public String removeApp(ChartVo chartVo) throws Exception;
	public String addNewApp(ChartVo chartVo) throws Exception;
	public String login(ChartVo chartVo) throws Exception;
	public String getStartTime(ChartVo chartVo) throws Exception;
	public String getComName(ChartVo chartVo) throws Exception;
	public ChartVo getBanner(ChartVo chartVo) throws Exception;
	public ChartVo getSummaryData(ChartVo chartVo) throws Exception;
	public String getBarChartData(ChartVo chartVo) throws Exception;
	public String getSpindleData(ChartVo chartVo) throws Exception;
	
};
