
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String lastAlarmCode;
	String lastAlarmMsg;
	String date;
	String chartStatus;
	String OperationTime;
	String lastProgramName;
	String lastProgramHeader;
	String inCycleTime;
	int cuttingTime;
	String waitTime;
	String alarmTime;
	String noConnectionTime;
	String lastTgPrdctNum;
	String lastFnPrdctNum;
	String lastAvrCycleTime;
	String tgCnt;
	String prdctPerHour;
	String prdctPerCyl;
	String opRatio;
	String cuttingRatio;
	String alarm;
	String remainCnt;
	String ncAlarmNum1;
	String ncAlarmNum2;
	String ncAlarmNum3;
	String ncAlarmMsg1;
	String ncAlarmMsg2;
	String ncAlarmMsg3;
	String programHeader;
	String programName;
	String type;
	
	int offset;
	int maxRow;
	String sDate;
	String eDate;
	String status;
	String startDateTime;
	String endDateTime;
	String alarmCode;
	String alarmMsg;
	String spdLoad;
	String spdOverride;
	String feedOverride;
	String workDate;
	
	//common
	String url;
	String fileLocation;
	String ty;
	Integer shopId;
	String fileName;
	String appId;
	String appName;
	String categoryId;
	Integer dvcId;
	String id;
	String pwd;
	String msg;
	String rgb;
	String name;
	int timeDiff;
	
	String prgmHead;
	String mainPrgmName;
	
	String currentTime;
	
}
