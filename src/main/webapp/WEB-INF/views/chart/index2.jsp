<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">
<script src="${ctxPath }/js/moment.min.js"></script>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script> 

<script src="${ctxPath }/js/solid-gauge.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-rotate.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">

	var barchartSeries = {
		
	}
	
	var dvcIdx = 0;
	var first = true;
	var shopId=2;
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		console.log(url+param)
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='-1'>Auto</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.dvcId)
					list += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#dvcId").html(list);
				
				$("#dvcId").css({
					"font-size" : getElSize(70),
					//"margin-bottom" : getElSize(10),
					"width" : getElSize(500),
					"-webkit-appearance": "none",
				    "-moz-appearance": "none",
			    	"appearance": "none",
			    	//"margin-top" : getElSize(100)
				});
				
				if(first){
					//localstorage에 저장 된 dvcid get
					for(var i = 0; i < dvcArray.length; i++){
						if(selectedDvcId == dvcArray[i]){
							dvcIdx = i;
						}
					}
					
					first = false;
				}
				
				dvcId = dvcArray[dvcIdx];
				getDvcData()
				$("#dvcId").val(dvcId);
				
				dvcIdx++;
				if(dvcIdx>=dvcArray.length) dvcIdx=0;
				
				//getDvcId();
				getCurrentDvcStatus(dvcId)
			}
		});
	};
	
	function getCurrentDvcStatus(dvcId){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = $("#today").val();
		
		var sDate = moment(today)
		var eDate = moment(today)
		
		sDate = sDate.hour(18).minute(00)
		sDate = sDate.subtract(1,'days')
		sDate = sDate.format("YYYY-MM-DD HH:mm")
		
		eDate = eDate.hour(18).minute(00)
		eDate = eDate.format("YYYY-MM-DD HH:mm")
		
		var url = "${ctxPath}/getSummaryData.do";
		var param = "dvcId=" + dvcId + 
					"&sDate=" + sDate+
					"&eDate=" + eDate;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				
				var chartStatus = data.chartStatus;
				var type = data.type;
				var progName = data.lastProgramName;
				var progHeader = data.prgmHead;
				var name = data.name;
				var inCycleTime = data.inCycleTime;
				var cuttingTime = data.cuttingTime;
				var waitTime = data.waitTime;
				var alarmTime = data.alarmTime;
				var noConTime = data.noConnectionTime;
				var spdLoad = data.spdLoad;
				var feedOverride = data.feedOverride;
				var opRatio = ((parseInt(cuttingTime)+parseInt(inCycleTime))/(parseInt(cuttingTime)+parseInt(inCycleTime)+parseInt(alarmTime)+parseInt(waitTime)))*100;
				var cuttingRatio = cuttingTime/(parseInt(inCycleTime)+parseInt(cuttingTime)) * 100;
				var alarm = data.alarm;
				var lastProgramName = data.lastProgramName;
				var lastProgramHeader = data.lastProgramHeader;
				
				console.log(data)
				
				if(isNaN(opRatio)){
					opRatio=0
				}
				if(isNaN(cuttingRatio)){
					cuttingRatio=0
				}
				
				if(data.waitTime==0 && data.inCycleTime==0 && data.cuttingTime==0 && data.noConnectionTime==0 && data.alarmTime==0){
					if($("#today").val()!=moment().format("YYYY-MM-DD")){
						noConTime=86400;
					}else if($("#today").val()==moment().format("YYYY-MM-DD")){
						var startTime = moment(sDate)
						noConTime = moment().diff(startTime, "seconds")
					}
					
				}
				
				if(lastProgramName==null)lastProgramName = "-"
				if(lastProgramHeader==null)lastProgramHeader = "-"
					
				var prgName = "[" + lastProgramName + "] " + lastProgramHeader;
				
				$("#programName").html(prgName).css({
					"color" : "white",
					"font-size" :getElSize(70),
					"position" : "absolute",
					"z-index" : 9,
					"top" : getElSize(250) + marginHeight,
					"left" : getElSize(1200) + marginWidth
				});
				
				
				$("#program_div").html(prgName).css({
					"color" : "white",
					"font-size" :getElSize(50),
					"margin" : getElSize(20) + "px"
				});
				
				if(data.chartStatus=="null"){
					name = window.sessionStorage.getItem("name");
				}
				
				//spd, feed
				$("#opratio_gauge").empty();
				$("#opratio_gauge").circleDiagram({
					textSize: getElSize(50), // text color
					percent : Math.round(opRatio) + "%",
					size: getElSize(200), // graph size
					borderWidth: getElSize(20), // border width
					bgFill: "#353535", // background color
					frFill: "#0080FF", // foreground color
					//font: "serif", // font
					textColor: '#0080FF' // text color
				});
				
				$("#cutting_gauge").empty();
				$("#cutting_gauge").circleDiagram({
					textSize: getElSize(50), // text color
					percent : Math.round(cuttingRatio) + "%",
					size: getElSize(200), // graph size
					borderWidth: getElSize(20), // border width
					bgFill: "#353535", // background color
					frFill: "#0080FF", // foreground color
					//font: "serif", // font
					textColor: '#0080FF' // text color
				});
						
				
				$("#opratio_span").html("${op_ratio}").css({
					"top" : getElSize(1780),
					"color" : "#8D8D8D",
					"left" :$("#opratio_gauge").offset().left + ($("#cutting_gauge").width()/2) - ($("#opratio_span").width()/2)- marginWidth
				});
				
				//io logik이 아닐 때
				if(String(type).indexOf("IO")==-1){
					$("#cutting_gauge, #cuttingratio_span").css({"display": "block"})
					$("#cuttingratio_span").html("${cuttingratio}").css({
						"top" : getElSize(1780),
						"color" : "#8D8D8D",
						"left" :$("#cutting_gauge").offset().left + ($("#cutting_gauge").width()/2) - ($("#cuttingratio_span").width()/2)- marginWidth
					});
				}else{
					$("#cutting_gauge, #cuttingratio_span").css({"display": "none"})
				}
				
				var alarm_time = (alarmTime/60/60).toFixed(1);
				if(alarmTime!=0 && alarm_time == "0.0"){
					alarm_time = "0.1";
				}
				
				$("#cutSpan").html((cuttingTime/60/60).toFixed(1));
				$("#inCycleSpan").html((inCycleTime/60/60).toFixed(1));
				$("#waitSpan").html((waitTime/60/60).toFixed(1));
				$("#alarmSpan").html(alarm_time);
				$("#noConnSpan").html((noConTime/60/60).toFixed(1));
				
				pieChart.series[0].data[0].update(Number((inCycleTime/60/60).toFixed(1)))
				pieChart.series[0].data[1].update(Number((cuttingTime/60/60).toFixed(1)))
				pieChart.series[0].data[2].update(Number((waitTime/60/60).toFixed(1)))
				pieChart.series[0].data[3].update(Number(alarm_time))
				pieChart.series[0].data[4].update(Number((noConTime/60/60).toFixed(1)))
				
				getDetailData()
				setInterval(function(){
					getCurrentDvcStatus(dvcId)					
				}, 1000*60*10)
			}
		});
	};
	
	function getDvcData(){
		//getMachineName();
		/* getDetailData();
		getAlarmList();
		getRapairDataList(); */
		getStartTime();
		
		//setTimeout(getDvcData, 5000);
	};
	
	var handle = 0;
	var selectedDvcId;
	
	/* function drawGauge(){
		$("#gauge").css({
			"width" : getElSize(900),
			"height" : getElSize(380)
		})
		
		var gaugeOptions = {

			    chart: {
			        type: 'solidgauge',
			        backgroundColor : "rgba(0,0,0,0)",
			        marginBottom : 0,
			        marginTop : -getElSize(460)
			        
			    },

			    
			    title: null,

			    pane: {
			        center: ['50%', '100%'],
			        size: '90%',
			        startAngle: -90,
			        endAngle: 90,
			        background: {
			            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
			            innerRadius: '60%',
			            outerRadius: '100%',
			            shape: 'arc'
			        }
			    },

			    tooltip: {
			        enabled: false
			    },

			    // the value axis
			    yAxis: {
			        stops: [
			            [0.1, '#55BF3B'], // green
			            [0.5, '#DDDF0D'], // yellow
			            [0.9, '#DF5353'] // red 
			            [1, '#55BF3B'], // green
			        ],
			        lineWidth: 0,
			        minorTickInterval: null,
			        tickAmount: 2,
			        title: {
			            y: -70
			        },
			        labels: {
			            y: 16
			        }
			    },

			    plotOptions: {
			        solidgauge: {
			            dataLabels: {
			                y: 5,
			                borderWidth: 0,
			                useHTML: true
			            }
			        }
			    }
			};

			// The speed gauge
			$('#gauge').highcharts(Highcharts.merge(gaugeOptions, {
			//var chartSpeed = Highcharts.chart('gauge', Highcharts.merge(gaugeOptions, {
			    yAxis: {
			    	min: 0,
					max: 100,
			        title: {
			            text: ''
			        }
			    },

			    credits: {
			        enabled: false
			    },

			    series: [{
			        name: 'Speed',
			        data: [0],
			        dataLabels: {
			            format: '<div style="text-align:center"><font style="font-size:25px; color:' +
			                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'blue') + '">{y}%</font>'
			        },
			        tooltip: {
			            valueSuffix: ' km/h'
			        }
			    }]

			}));
	}; */
	
	function changeDateVal(){
		window.sessionStorage.setItem("date", $("#today").val())		
		var now = new Date(); 
		var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		
		var date = new Date();
		
		var year = date.getFullYear();
		var month = date.getMonth()+1;
		var day = date.getDate();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		// Set specificDate to a specified date at midnight.
		
		var selectedDate = $("#today").val();
		var s_year = selectedDate.substr(0,4);
		var s_month = selectedDate.substr(5,2);
		var s_day = selectedDate.substr(8,2);

		var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
		
		var time = year + "-" + month + "-" + day;
		
		var today = getToday().substr(0,10);
		
		/* if(todayAtMidn.getTime()<specificDate.getTime()){
			alert("오늘 이후의 날짜는 선택할 수 없습니다.");
			$("#today").val(today);
			return;
		}; */
		
		
		if(today==$("#today").val()){
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth() + 1));
			var day = addZero(String(date.getDate()));
			var hour = date.getHours();
			var minute = addZero(String(date.getMinutes())).substr(0,1);
			
			
			if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
				day = addZero(String(new Date().getDate()+1));
			};
			
			
			selectedDate = year + "-" + month + "-" + day;
			
		}
		
		//drawBarChart("timeChart", selectedDate);
		getTimeData()
		getDetailData();
		getCurrentDvcStatus(dvcId);

	};
	
	function upDate(){
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		changeDateVal();
	};
	
	function downDate(){
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		changeDateVal();
	};
	
	var appId = 31;
	
	var addAppFlag = true;
	var categoryId = 1;
	
	//var appServerUrl = "http://localhost:8080/App_Store/index.do";
	var appServerUrl = "http://52.169.201.78:8080/App_Store/index.do?categoryId=1";
	window.addEventListener("message", receiveMessage, false);
	
	function receiveMessage(evet){
		var msg = event.data;
		
		$("#app_store_iframe").animate({
			"left" : - $("#app_store_iframe").width() * 1.5 
		}, function(){
			$("#app_store_iframe").css({
				"left" : originWidth,
				"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
			})
			
			addAppFlag = true;
			$(".addApp").rotate({
				duration:500,
			    angle: 45,
			   	animateTo:0
			});
		});
		
		fileDown(msg.url, msg.appName, msg.appId)
	}
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	/* function fileDown(appUrl, appName, appId){
		var url = "${ctxPath}/fileDown.do";
		var param = "fileLocation=" + appUrl + 
					"&fileName=" + appName + 
					"&appId=" + appId + 
					"&ty=" + categoryId + 
					"&shopId=" + shopId;

		getProgress();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				if(data=="success"){
					getAppList()					
				}
			}, error : function(e1,e2,e3){
			}
		});
	}; */
	
	var progressLoop = false;
	function getProgress(){
		var url = "${ctxPath}/getProgress.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "text",
			success : function(data){
				
				
				if(data<100){
					$(".progressBar").remove();
					//draw ProgressBar();
					var targetBar = $(".nav_span").next("img").width();
					var currentBar = targetBar * Number(data).toFixed(1)/100;
					
					
					var barHeight = getElSize(50);
					var _top = $(".nav_span:nth(" + appCnt + ")").parent("td").offset().top + $(".nav_span:nth(" + appCnt + ")").parent("td").height() - barHeight;
					var _left = $(".nav_span:nth(" + appCnt + ")").parent("td").offset().left;
					
					var bar = document.createElement("div");
					bar.setAttribute("class", "progressBar");
					bar.style.cssText = "width : " + currentBar + "px;" + 
										"height : " + barHeight + "px;" +
										"position : absolute;" + 
										"top : " + _top + "px;" + 
										"color : #8D8D8D;" +
										"left : " + _left + "px;" +
										"z-index : 99999;" + 
										"background-color : lightgreen;";
										
										
					$("body").prepend(bar);
					
					clearInterval(progressLoop);
					progressLoop = setTimeout(getProgress,500)
					
					$(".nav_span:nth(" + appCnt + ")").html(Number(data).toFixed(1) + "%")	 
				}else{
					resetProgress();
				}
				
				$(".addApp").remove();
			}
		});
	};
	
	function resetProgress(){
		var url = "${ctxPath}/resetProgress.do";
		
		$.ajax({
			url : url,
			type : "post",
			success : function(data){
			}
		});
	};
	
	
	function showMinusBtn(parentTd, appId){
		$(".minus").animate({
			"width" : 0
		}, function(){
			$(this).remove();
		});
		
		var img = document.createElement("img");
		img.setAttribute("src", "${ctxPath}/images/minus.png");
		img.setAttribute("class", "minus");
		img.style.cssText = "position : absolute;" +
							"cursor : pointer;" + 
							"z-index: 9999;" + 
							"left : " + (parentTd.offset().left + parentTd.width()) + "px;" +
							"top : " + (parentTd.offset().top) + "px;" +
							"width : " + getElSize(0) + "px;";
		
		$(img).click(function(){
			$("#delDiv").animate({
				"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
				"top" : (window.innerHeight/2) - ($("#delDiv").height()/2) + getElSize(100)
			}, function(){
				$("#delDiv").animate({
					"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
					"top" : (window.innerHeight/2) - ($("#delDiv").height()/2)
				}, 100)
			});
			
			$("#delDiv div:nth(0)").click(function(){
				removeApp(appId);
			})
		});
		
		$(img).animate({
			"width" : getElSize(80)
		});		
		
		$("body").prepend(img)					
	};
	
	/* function removeApp(appId){
		var url = "${ctxPath}/removeApp.do";
		var param = "appId=" + appId + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					$("#delDiv").animate({
						"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
						"top" : - $("#delDiv").height() * 2
					})	
					
					$(".minus").animate({
						"width" : 0
					}, function(){
						$(this).remove();
					});
					getAppList();
				}
			}
		});
	}; */
	
	/* var appArray = [];
	var appCnt = 0;
	function getAppList(){
		clearInterval(progressLoop);
		$(".progressBar").remove();
		
		var url = "${ctxPath}/getAppList.do";
		var param = "categoryId=" + categoryId + 
					"&shopId=" +shopId;
		
		appArray = [];
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
		
				$(".nav_span").html("");
				$(".addApp").remove();
				
				//id, appId, name
				
				appCnt = json.length;
				
				$(".nav_span").next("img").unbind("contextmenu").attr("src", "${ctxPath}/images/unselected.png");
				$(".nav_span").unbind("contextmenu");
				
				$(json).each(function(idx, data){
					var appName;
					if(data.name=="DashBoard_EMO"){
						appName = "${dashboard_emo}"
					}else if(data.name=="DashBoard_Doosan"){
						appName = "${dashboard_doosan}"
					}else if(data.name=="DashBoard_BK"){
						appName = "${dashboard_bk}";
					}else if(data.name=="DashBoard2_BK"){
						appName = "${dashboard_bk2}";
					}else if(data.name=="Single_Chart_Status"){
						appName = "${dailydevicestatus}";
					}else if(data.name=="Production_Board"){
						appName = "${prdct_board}";
					}else if(data.name=="24hChart"){
						appName = "${barchart}";
					}else if(data.name=="DMM"){
						appName = "${dmm}";
					}
					
					appArray.push(data.appId)
					$(".nav_span:nth(" + idx + ")").attr("appId", data.appId);
					$(".nav_span:nth(" + idx + ")").html(appName);
					$(".nav_span:nth(" + idx + ")").click(function(){
						location.href = "http://" + data.url + "?lang=" + window.localStorage.getItem("lang");
					});
					
					$(".nav_span:nth(" + idx + ")").next("img").click(function(){
						 if(data.name=="DashBoard"){
							location.href = "/DIMF/chart/main.do?categoryId=1";	
						}else{
							location.href = "/" + data.name + "/index.do";	
						} 
							
						location.href = "http://" + data.url + "?lang=" + window.localStorage.getItem("lang");
					});
					
					$(".nav_span:nth(" + idx + ")").next("img").attr("appId", data.appId)
					
					if(data.appId==appId){
						$(".nav_span:nth(" + idx + ")").next("img").attr("src", "${ctxPath}/images/selected_blue.png")
						$(".nav_span:nth(" + idx + ")").css("color", "white");
					}
					$(".nav_span:nth(" + idx + ")").next("img").contextmenu(function(e){
						e.preventDefault();
						var appId = $(this).prev("span").attr("appId");
						var parentTd = $(".nav_span:nth(" + idx + ")").parent("Td");
						showMinusBtn(parentTd, appId)						
					});
					
					$(".nav_span:nth(" + idx + ")").contextmenu(function(e){
						e.preventDefault();
						var appId = $(this).attr("appId");
						var parentTd = $(".nav_span:nth(" + idx + ")").parent("Td")
						showMinusBtn(parentTd, appId)						
					});
				});
				
				var totalCell = 10;
				
				var parentTd = $(".nav_span:nth(" + json.length + ")").parent("td");
				var img = document.createElement("img");
				img.setAttribute("src", "${ctxPath}/images/add.png");
				img.setAttribute("class", "addApp");
				img.style.cssText = "position : absolute;" +
									"cursor : pointer;" + 
									"z-index: 9999;" + 
									"left : " + (parentTd.offset().left + (parentTd.width()/2) - getElSize(40)) + "px;" +
									"top : " + (parentTd.offset().top + (parentTd.height()/2) - getElSize(40)) + "px;" +
									"width : " + getElSize(80) + "px;"; 
									
					
				$("body").prepend(img);
				
				//iframe 호출
				$(img).click(function(){
					document.querySelector("#app_store_iframe").contentWindow.postMessage(appArray, '*');
					
					if(addAppFlag){
						$(this).rotate({
					   		duration:500,
					      	angle: 0,
					      	animateTo:45
						});
						$("#app_store_iframe").animate({
							"left" : (originWidth/2) - ($("#app_store_iframe").width()/2)
						});	
					}else{
						$(this).rotate({
							duration:500,
						    angle: 45,
						   	animateTo:0
						});
						$("#app_store_iframe").animate({
							"left" : - $("#app_store_iframe").width() * 1.5 
						}, function(){
							$("#app_store_iframe").css({
								"left" : originWidth,
								"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
							})
						});
					}
					
					addAppFlag = !addAppFlag;
				})
			}
		});
	}; */
	
	
	
	var sessionDate;
	var pieChart;
	function drawPieChart(){
		  // Build the chart
	    $('#pieChart').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            width : getElSize(350),
	            height : getElSize(350),
	            plotBorderWidth: null,
	            backgroundColor : "rgba(255,255,255,0)",
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: null
	        },
	        tooltip: {
				enabled :false,
	        	pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        legend : {
	        	enabled : false
	        },
	        exporting : false,
	        credits : false,
	        plotOptions: {
	            pie: {
	            	size: getElSize(350),
	            	//borderWidth: 0,
	                //allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: false
	                },
	                showInLegend: true
	            }
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            data: [{
	                name: 'In-Cycle',
	                y: 0,
	                color : incycleColor
	            }, {
	                name: 'Cut',
	                y: 0,
	                color : cuttingColor,
	            }, {
	                name: 'Wait',
	                y: 0,
	                color : waitColor
	            }, {
	                name: 'Alarm',
	                y: 0,
	                color : alarmColor
	            }, {
	                name: 'Off',
	                y: 0,
	                color : noconnColor
	            }]
	        }]
	    });
		  
	    pieChart = $('#pieChart').highcharts()
	};
	
	$(function(){
		drawPieChart();		
		//getAppList();
		
		var today = moment();
		if(today.hour()>=18){
			today = today.add(1, 'days')
		}
		today = today.format("YYYY-MM-DD")
		
		$("#today").val(today)
		
		$("#today").change(changeDateVal);
		$("#up").click(upDate);
		$("#down").click(downDate);
		
		//drawGauge();
		selectedDvcId = window.localStorage.getItem("dvcId");
		createNav("monitor_nav", 2)
		getDvcList();
		time();
		
		$("#dvcId").change(function(){
			dvcId = $("#dvcId").val();
			window.localStorage.setItem("dvcId", dvcId)
			if(this.value==-1){
				rotate_flag = true;	
			}else{
				rotate_flag = false;
				getDvcData();
				getCurrentDvcStatus(dvcId);
			}
		});
		
		
	
		setDivPos();
		
		/* window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10); */
		
		
		chkBanner();
	});
	
	var startTimeLabel = new Array();
	var startHour, startMinute;
	
	function getStartTime(){
		var url = ctxPath + "/getStartTime.do";
		var param = "shopId=" + shopId;;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				startHour = data.split("-")[0]
				startMinute = data.split("-")[1]

				var date = new Date();
				var year = date.getFullYear();
				var month = addZero(String(date.getMonth() + 1));
				var day = addZero(String(date.getDate()));
				var hour = date.getHours();
				var minute = addZero(String(date.getMinutes())).substr(0,1);
				
				
				if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
					day = addZero(String(new Date().getDate()+1));
				};
				
				
				var today = year + "-" + month + "-" + day;
				getTimeData()
			}, error : function(e1,e2,e3){
			}
		});	
	};
	
	var targetMinute = String(addZero(new Date().getMinutes())).substr(0,1);
	
	var incycleColor = "#50BA29";
	var cuttingColor = "#175501";
	var waitColor = "yellow";
	var alarmColor = "red";
	var noconnColor= "#A0A0A0";
	
	function getTimeData(options, today){
		
		
		$('#timeChart_td').loading({
			message: '${loading}',
			theme: 'dark'
		});
		
		var today = $("#today").val();
		
		var sDate = moment(today)
		var eDate = moment(today)
		
		sDate = sDate.hour(18).minute(00)
		sDate = sDate.subtract(1,'days')
		sDate = sDate.format("YYYY-MM-DD HH:mm")
		
		eDate = eDate.hour(18).minute(00)
		eDate = eDate.format("YYYY-MM-DD HH:mm")
		var currentTime = moment().format("YYYY-MM-DD HH:mm:ss")
		
		var url = ctxPath + "/getBarChartData.do";
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&dvcId=" + dvcId + 
					"&currentTime=" + currentTime;
		
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(data){
				$('#timeChart_td').loading('stop');
			
				var json = data.statusList;
				console.log("저속 JSON LIST")
				console.log(json)
				var color = "";
				
				if(json.length==0){
					status="NO-CONNECTION"
				}else{
					var status = json[0].status;
				}
				
				if(status=="CUT"){
					color = cuttingColor;
				}else if(status=="IN-CYCLE"){
					color = incycleColor;
				}else if(status=="WAIT"){
					color = waitColor;
				}else if(status=="ALARM"){
					color = alarmColor;
				}else if(status=="NO-CONNECTION"){
					color = noconnColor;
				};
				
				spdLoadPoint = [];
				spdOverridePoint = [];
				
				var dataSource =[];
				var lastTime =0;
				var checkFirst = 0;
				
				if(json.length==0){
					
					if(moment().format("YYYY-MM-DD")==$("#today").val()){
						var startTime = moment()
						startTime = startTime.hour(18)
						startTime = startTime.minute(0)
						startTime = startTime.second(0)
						startTime = startTime.subtract(1, 'days')
						var diffTime = moment().diff(startTime,'second')
						var element = {name: 'NO-DATA',color: noconnColor, data: [diffTime]}
						dataSource.push(element);
						dataSource.unshift({name:'NOT-YET', color:'rgba(0,0,0,0)', data:[86400-diffTime]})
					}else{
						var element = {name: 'NO-DATA',color: noconnColor, data: [86400]}
						dataSource.push(element);
					}
					
				}else{
					for(var i=0;i<json.length;i++){
						var statusColor='';
						if(json[i].dateDiff>0){
							
							lastTime+=moment(json[json.length-1].startDateTime).diff(moment(sDate),'seconds')
							if(json[i].status=='CUT'){
								var element = {name: json[i].status,color: cuttingColor, data: [json[i].dateDiff]}
							}else if(json[i].status=='IN-CYCLE'){
								var element = {name: json[i].status,color: incycleColor, data: [json[i].dateDiff]}
							}else if(json[i].status=='WAIT'){
								var element = {name: json[i].status,color: waitColor, data: [json[i].dateDiff]}
							}else if(json[i].status=='ALARM'){
								var element = {name: json[i].status,color: alarmColor, data: [json[i].dateDiff]}
							}else if(json[i].status=='NO-CONNECTION'){
								var element = {name: json[i].status,color: noconnColor, data: [json[i].dateDiff]}
							}
							lastTime+=json[i].dateDiff
							
							dataSource.push(element);
						}
						if(i==json.length-1){
							if(moment().format("YYYY-MM-DD")==$("#today").val()){
								dataSource.unshift({name:'NOT-YET', color:'rgba(0,0,0,0)', data:[86400-lastTime]})
							}
						}
					}
					if(moment(json[json.length-1].startDateTime).diff(moment(sDate),'seconds')>0){
						var element = {name: json[json.length-1].status,color: noconnColor, data: [moment(json[json.length-1].startDateTime).diff(moment(sDate),'seconds')]}
						lastTime+=moment(json[json.length-1].startDateTime).diff(moment(sDate),'seconds')
						dataSource.push(element);
					}
				}
				
				
				var perShapeGradient = {
					x1 : 0,
					y1 : 0,
					x2 : 1,
					y2 : 0
				};
				colors = Highcharts.getOptions().colors;
				colors = [ {
					linearGradient : perShapeGradient,
					stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
				}, {
					linearGradient : perShapeGradient,
					stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
				}, {
					linearGradient : perShapeGradient,
					stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
				}, {
					linearGradient : perShapeGradient,
					stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
				}, ]
				
				var options = {
						chart: {
			                type: 'bar',
			                animation: true,
			                backgroundColor : 'rgba(0,0,0,0)',
							height : getElSize(300),
					    	marginRight : getElSize(25),
					    	marginLeft : getElSize(50),
					    	marginBottom : getElSize(150),
					    	marginTop : getElSize(20),
					    	spacingBottom: 0
			            },
			            credits: { enabled: false },
			            exporting: { enabled: false },
			            title: {
			                text: null
			            },
			            xAxis: {
			            	title: {
				                text: null
				            },
				            categories:['']
			            },
			            yAxis: {
			                min: 0,
			                ceiling: 86400, // one day (3600*24)
			                title: {
				                text: null
				            },
			                tickInterval: 3600,
			                labels : {
			                	formatter : function() {
									var val = this.value
									if((val/3600)+18<25){
										return (val/3600)+18;
									}else{
										return ((val/3600)+18)-24;
									}
								},
								style : {
									color : "white",
									fontSize : getElSize(30),
									fontWeight : "bold"
								},
							}
			            },
			            legend: {
			                enabled: false
			            },
			            tooltip : {
							enabled : false
						},
			            plotOptions: {
			                series: {
			                    stacking: 'normal',
			                    animation: true,
			                    boostThreshold:1
			                },
			                bar: {
			                    animation: true,
			                    borderWidth:0,
			                    enableMouseTracking: false,
			                    boostThreshold:1
			                }
			            },
			            series:dataSource
					};
					
				console.time('area');
					$("#timeChart").highcharts(options);
				console.timeEnd('area');
			
				getSpdFeed();
			},error : function(e1,e2,e3){
			}
		});
	};
	
	
	var spdOverridePoint = [];
	
	function getSpdFeed(){
		
		var sDate = moment($("#today").val())
		var eDate = moment($("#today").val())
		
		sDate = sDate.hour(18).minute(00)
		sDate = sDate.subtract(1,'days')
		sDate = sDate.format("YYYY-MM-DD HH:mm")
		
		eDate = eDate.hour(18).minute(00)
		eDate = eDate.format("YYYY-MM-DD HH:mm")
		
		var url = ctxPath + "/getSpindleData.do";
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&dvcId=" + dvcId; 
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var spdLoadPoint = [];
				var json = data.statusList;
				
				console.log("Spindle Load Data")
				console.log(json)
				
				try{
					catchException=true;
					var f_Hour = json[0].startDateTime.substr(11,2);
					var f_Minute = json[0].startDateTime.substr(14,2);
				}catch(err){
					
					console.log("Spindle Load Exception Error")
					console.log(err)
					catchException=false;
					var nowTime = 0;
					
					var now = moment();
					
					if(moment().hour()>18){
						now = moment().add(1,'days');
					}
					if($("#today").val()==now.format("YYYY-MM-DD")){
						var startTime = moment(sDate);
						nowTime = moment().diff(startTime, 'minutes')/2						
						for(var i = 0; i < nowTime-720 ; i++){
							spdLoadPoint.push(Number(0));
						};
					}else{
						for(var i = 0; i < 719 - nowTime; i++){
							spdLoadPoint.push(Number(0));
						};
						
						for(var i = 0; i < nowTime-2; i++){
							spdLoadPoint.push(Number(0));
						}
					}
				}
				
				if(catchException){
					var startN = 0;
					if(f_Hour==startHour && f_Minute==(startMinute*10)){
					}else{
						if(f_Hour>=Number(startHour)){
							startN = (((f_Hour*60) + Number(f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
						}else{
							startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
							startN += (f_Hour*60/2) + (f_Minute/2);
						};
						for(var i = 0; i < startN-1; i++){
							spdLoadPoint.push(Number(0));
							spdOverridePoint.push(0)
						};
					};
					for(var i=0;i<json.length;i++){
						spdLoadPoint.push(Number(json[i].spdLoad));
						spdOverridePoint.push(Number(json[i].spdOverride))
					}
				}
				console.log(spdLoadPoint.length)
				for(var i=spdLoadPoint.length;i<720;i++ ){
					spdLoadPoint.push(-10)	
					spdOverridePoint.push(-10)
				}
				
				$("#legend").css("display", "inline");
				$('#spdFeed').highcharts({
			        chart: {
			            type: 'line',
			            backgroundColor : "rgba(0,0,0,0)",
			            height : getElSize(300),
			          //  width : getElSize(1000)
			        },
			        exporting : false,
			        credits : false,
			        title: {
			            text: null
			        },
			        subtitle: {
			            text: null
			        },
			        legend : {
			        	enabled : false,
			        	itemStyle : {
			        			color : "white",
			        			fontWeight: 'bold'
			        		}
			        },
			        xAxis: {
		                min: 0,
		                ceiling: 720, // one day (3600/2)
		                title: {
			                text: null
			            },
		                tickInterval: 30,
		                labels : {
		                	formatter : function() {
								var val = this.value
								return ;
							},
							style : {
								color : "white",
								fontSize : getElSize(30),
								fontWeight : "bold"
							},
						}
		            },
			        yAxis: [{
			        	min : 0,
			        	gridLineWidth: 1,
						minorGridLineWidth: 0,
			            title: {
			                text: null
			            },
			            labels : {
			            	 useHTML: true,
			                 formatter: function() {
			                     return "<span style='color:red; background-color:black; padding:" + getElSize(5) + "px'>" + this.value + "</span>";
			                 },
			            	x: getElSize(50),
			            }
			        },{
			        	min : 0,
			        	opposite: true,
			        	gridLineWidth: 1,
			            title: {
			                text: null
			            },
			            labels : {
			            	useHTML: true,
			            	formatter: function() {
			                     return "<span style='color:yellow; background-color:black; padding:" + getElSize(5) + "px'>" + this.value + "</span>";
			                 },
			            	x: -getElSize(50),
			            }
			        }],
			        plotOptions: {
			            line: {
			                dataLabels: {	
			                    enabled: false,
			                    color: 'white',
			                    style: {
			                        textShadow: false 
			                    }
			                },
			                enableMouseTracking: false
			            }
			        },
			        series: [{
			            		name: 'Spindle Load',
			            		color : 'yellow',
			            		data : spdLoadPoint,
			            		lineWidth : getElSize(10),
			            		marker : {
		                    			enabled : false,
		                		},
		                		yAxis: 1
			        		},
			        		{
			            		name: 'Spindle Override',
			            		color : 'red',
			            		data: spdOverridePoint,
			            		lineWidth : getElSize(10),
			            		marker : {
		                    		enabled : false,
		                		},
			        		}]
			    });	
				spdChart = $('#spdFeed').highcharts();
			}
		})
		
	};
	
	function drawPrdctChart(ratio){
		$("#target_ratio").html(Math.round(ratio) + "%").css({
			"color" : "#0080FF",
			"font-size" : getElSize(100),
			"right" : getElSize(30),
			"top" : getElSize(850)
		});
		
		$('#prdctChart').highcharts({
		    chart: {
		    	backgroundColor : "rgba(0,0,0,0)",
		    	height : getElSize(230),
		    	marginRight : getElSize(50),
		    	marginLeft : getElSize(50),
		        type: 'bar'
		    },
		    credits : false,
		    exporting : false,
		    title: {
		        text: null
		    },
		    xAxis: {
		    	/* gridLineWidth:0,
		    	lineWidth: 0,*/
		    	tickWidth: 0, 
				labels: {
		       		enabled: false
		    	},    
		    },
		    yAxis: {
			    gridLineWidth: 0,
				minorGridLineWidth: 0,
		        min: 0,
		        max :100,
				labels : {
					style : {
						color : "#7A7B7C",
						fontSize :getElSize(30)
					}
		        },
		        title: {
		            text: null
		        }
		    },
		    tooltip : {
				enabled : false
			},
		    legend: {
		        reversed: true,
		        enabled : false
		    },
		    plotOptions: {
		        series: {
		            stacking: 'normal',
		            groupPadding: 0,
		            pointPadding: 0,
		            borderWidth: 0
		        }
		    },
		    /* series: [{
		    		color : "#BBBDBF",
		        name: 'target',
		        data: [10]
		    },{
		    		color : "#A3D800",
		        name: 'goal',
		        data: [10]
		    }] */
		    series: [{
	    		color : "#A3D800",
	        name: 'goal',
	        data: [Math.round(ratio)]
	    }]
		});			
	};
	
	function getAlarmList(){
		var url = "${ctxPath}/getAlarmList.do";
		
		var sDate = moment($("#today").val())
		var eDate = moment($("#today").val())
		
		sDate = sDate.hour(18).minute(00)
		sDate = sDate.subtract(1,'days')
		sDate = sDate.format("YYYY-MM-DD HH:mm")
		
		eDate = eDate.hour(18).minute(00)
		eDate = eDate.format("YYYY-MM-DD HH:mm")
		
		
		var param = "dvcId=" + dvcId + 
					"&sDate=" + sDate + 
					"&eDate=" + eDate;
	
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				console.log(json)
	
				var alarm = "";
				
				
				$(json).each(function(idx, data){
					alarm += "[" + data.startDateTime.substr(5,14) + "] - " +data.alarmCode+" "+  decodeURIComponent(data.alarmMsg).replace(/\+/gi, " ")  + "<br><br>";
				});
	
				if(alarm!=""){
					alarm = "<font style='color:red' id='alarm_span'>-Alarm</font><br><div style='height:" + getElSize(250) + "px; overflow:auto;'>" + alarm + "</div>";
				}
				
				$("#alarm_div").html(alarm).not("#alarm_span").css({
					"color" : "white",
					"margin" : getElSize(20) + "px"
				});
			}
		});

	};
	
	function getDetailData(){
		var url = ctxPath + "/getDetailBlockData.do";
		var param = "dvcId=" + dvcId + 
					"&sDate=" + $("#today").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				
				var json = data.dataList;
				
				var alarm = "";
				
				getAlarmList();
				if(json.length==0){
					$("#cylCnt").html("-");
					$("#prdctPerCyl").html("-");
					$("#daily_target_cycle").html("-");
					$("#complete_cycle").html("-");
					$("#daily_avg_cycle_time").html("-");
					$("#daily_length").html("-");
					$("#feedOverride").html("-");
					$("#spdLoad").html("-");
					$("#downValue").html("-");
				};
				
				$(json).each(function(idx, data){
					var hour = new Date().getHours()-8;
					if(hour==0) hour=1;
					 
					var sign = "";
					
					
					$("#cylCnt").html(Math.round(data.lastFnPrdctNum/data.prdctPerCyl));
					$("#prdctPerCyl").html(data.prdctPerCyl);
					$("#daily_target_cycle").html(data.tgCnt);
					$("#complete_cycle").html(data.lastFnPrdctNum);
					$("#daily_avg_cycle_time").html(Number(data.LastAvrCycleTime/60).toFixed(1));
					$("#daily_length").html(Number(data.prdctPerHour).toFixed(1));
					$("#feedOverride").html(Number(data.feedOverride));
					$("#spdLoad").html(Number(data.spdLoad));
					
					//var remainCnt = data.remainCnt;
					var remainCnt = data.tgCnt - (data.lastFnPrdctNum);
					var color = "";
					if(Number(remainCnt)>0){
						sign = "-";
						color = "red"
					}else{
						sign = "+";
						remainCnt = Math.abs(remainCnt);
						color = "blue";
					};
					
					$("#downValue").html(sign + remainCnt).css({
						"font-size" : getElSize(70),
						"color" : color
					})
					
					
					//일 생산 현황
					//drawPrdctChart(data.lastFnPrdctNum / data.tgCnt * 100);
					/* var prdctChart = $("#gauge").highcharts().series[0].points[0];
					prdctChart.update(Number(Number(data.lastFnPrdctNum / data.tgCnt * 100).toFixed(1))); */
					
					
					$("#gauge").css({
					}).empty();
					
					
					
					var prdRatio = Number(Number(data.lastFnPrdctNum / data.tgCnt * 100).toFixed(1));
					
					if(data.tgCnt==0){
						prdRatio = 0
					}
					
					$("#gauge").circleDiagram({
						textSize: getElSize(70), // text color
						percent : prdRatio + "%",
						size: getElSize(300), // graph size
						borderWidth: getElSize(50), // border width
						bgFill: "#353535", // background color
						frFill: "#0080FF", // foreground color
						//font: "serif", // font
						textColor: '#0080FF' // text color
					});
					
					$("#gauge").css("display", "block")
					var n = (data.lastFnPrdctNum)/data.tgCnt*100;
					//barChart.series[0].data[0].update(Number(Number(n).toFixed(1)));
					
					if(String(data.type).indexOf("IO")!=-1){
						$("#gauge").css("display", "none")
						$("#cylCnt").html("-");
						$("#prdctPerCyl").html("-");
						$("#daily_target_cycle").html("-");
						$("#complete_cycle").html("-");
						$("#daily_avg_cycle_time").html("-");
						$("#daily_length").html("-");
						$("#feedOverride").html("-");
						$("#spdLoad").html("-");
						$("#downValue").html("-");
					}
				});
				
				
				$("#downValue").css({
					"position" : "absolute",
					"top" : getElSize(350)
				});

				$("#downValue").css({
					"left" : getElSize(1130),
				});
				
				//setTimeout(getDetailData, 5000)
			}
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(35),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$(".content").css({
			"height" : getElSize(400),
			"background-color" : "#191919",
			"color" : "white",
			"text-align" : "center",
			"font-size" : getElSize(170),
		});
		
		$("#prdctChart_td").css({
			"height" : getElSize(400),
			"background-color" : "#191919",
		});
		
		$("#timeChart_td").css({
			"height" : getElSize(700),
			"background-color" : "#191919"
		});
	
		$("#timeChart").css({
			"margin-top" : getElSize(50)	
		});
		
		$("#dvcId").css({
			"position" : "absolute",
			"font-size" : getElSize(70),
			"margin-top" : getElSize(25),
			"margin-left" : getElSize(30)
		});
		
		$("#statusChart").css({
			"z-index" : 99999,
			"position" : "absolute",
			"left" : getElSize(540),
			"width" : getElSize(750),
			"margin-top" : getElSize(150)
		});
		
		$("#legend").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"margin-left" : getElSize(1600),
			"display" : "none"
		});
		
		$("#spdLoadLine").css({
			"background-color" : "#F9FF00",			
			"width" : getElSize(90),
			"height" : getElSize(20)
		});
		
		$("#spdOvrrdLine").css({
			"background-color" : "#FF0000",
			"width" : getElSize(90),
			"height" : getElSize(20),
			"margin-left" : getElSize(50)
		});
		
		$("#cutTime, #inCycleTime, #waitTime, #alarmTime, #noConnTime").css({
			"color" : "black",
			"z-index" : 999999,
			"font-size" : getElSize(35)
		});
		
		//1043
		
		$("#inCycleTime").css({		
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(827) - marginHeight,
			"left" : getElSize(960)
		});
		
		$("#cutTime").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(885) - marginHeight,
			"left" : getElSize(960)
		});
		
		$("#waitTime").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(948) - marginHeight,
			"left" : getElSize(960)
		});
		
		$("#alarmTime").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(1011) - marginHeight,
			"left" : getElSize(960)
		});
		
		$("#noConnTime").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(1074) - marginHeight,
			"left" : getElSize(960)
		});
		
		$("#cutSpan, #inCycleSpan, #waitSpan, #alarmSpan, #noConnSpan").css({
			"z-index" : 999999,
			"color" : "black",
			"font-size" : getElSize(35),
			"position" : "absolute"
		});
		
		$("#inCycleSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(829) - marginHeight,
			"left" : getElSize(1110)
		});
		
		$("#cutSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(887) - marginHeight,
			"left" : getElSize(1110)
		});
		
		$("#waitSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(950) - marginHeight,
			"left" : getElSize(1110)
		});
		
		$("#alarmSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(1015) - marginHeight,
			"left" : getElSize(1110)
		});
		
		$("#noConnSpan").css({
			"top" : $("#main_table td:nth(9)").offset().top + getElSize(1077) - marginHeight,
			"left" : getElSize(1110)
		});
		
		$("#opratio_gauge").css({
			"margin-left" : getElSize(100),
			"margin-top" : getElSize(670)
		});
		
		$("#cutting_gauge").css({
			"margin-right" : getElSize(100),
			"margin-top" : getElSize(670)
		});
		$("img").css({
			"display" : "inline"
		})
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#date_table").css({
			"position" : "absolute",
			"top" :getElSize(200),
			"right" : getElSize(50)
		});
		
		$("#date_table button").css({
			"width" : getElSize(100),
			"height" : getElSize(60),
		});
		
		$("#today").css({
			"font-size" : getElSize(30) + "px",
			"height" : getElSize(60)
		})
		
		$("#pieChart").css({
			"z-index" : -20,
			"position" : "absolute",
			"width" : getElSize(500) + "px",
			"height" : getElSize(500) + "px",
			"z-index" : 99999,
			"top" : $("#statusChart").offset().top + getElSize(10),
			"left" : $("#statusChart").offset().left + getElSize(5)
		});
		
		$("#app_store_iframe").css({
			"width" : getElSize(3100) + "px",
			"height" : getElSize(2000) + "px",
			"position" : "absolute",
			"z-index" : 9999,
			"display" : "block"
		});
		
		$("#app_store_iframe").css({
			"left" : originWidth * 1.5,
			"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
		}).attr("src", appServerUrl)
		
		
		$("#delDiv").css({
			"color" : "white",
			"z-index" : 9999999,
			"background-color" : "black",
			"position" : "absolute",
			"width" : getElSize(700) + "px",
			"font-size" : getElSize(60) + "px",
			"text-align" : "center",
			"padding" : getElSize(30) + "px",
			"border" : getElSize(7) + "px solid rgb(34,34,34)",
			"border-radius" : getElSize(50) + "px"
		});
		
		
		$("#delDiv div").css({
			"background-color" : "rgb(34,34,34)",
			"padding" : getElSize(20) + "px",
			"margin" : getElSize(10) + "px",
			"cursor" : "pointer"
		}).hover(function(){
			$(this).css({
				"background-color" : "white",
				"color" : "rgb(34,34,34)",
			})
		}, function(){
			$(this).css({
				"background-color" : "rgb(34,34,34)",
				"color" : "white",
			})
		});
		
		$("#delDiv").css({
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"top" : - $("#delDiv").height() * 2
		});
		
		$("#delDiv div:nth(1)").click(function(){
			$("#delDiv").animate({
				"top" : - $("#delDiv").height() * 2
			});
		});
		
		$("#up, #down").css({
			"width" : getElSize(70) *  1.2,
			"height" : getElSize(70),		
		})
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	$.datepicker.setDefaults({
	    dateFormat: 'yy-mm-dd',
	    prevText: '이전 달',
	    nextText: '다음 달',
	    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
	    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
	    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
	    showMonthAfterYear: true,
	    yearSuffix: '년'
	});
	
	$(function() {
	    	/* $("#today").datepicker({
	    		onSelect:function(){
	    			changeDateVal()
	    		}
	    	}) */
	    	
	});
	
</script>
</head>
<body>
	<div id="delDiv">
		<spring:message  code="chk_del"></spring:message>
		<div><spring:message  code="check"></spring:message></div>
		<div><spring:message  code="cancel"></spring:message></div>
	</div>
	
	<iframe id="app_store_iframe"  style="display: none"></iframe>
	
	<div id="pieChart"></div>
	
	<div id="programName"></div>
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					<select id="dvcId"></select>
					<img alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table  style="width: 100%" id="main_table">
						<tr>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="dailyprdcttarget"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="dailyprdctcnt"></spring:message>	
								<span id="downValue"></span>
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="prdct_per_cycle"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_cycle_cnt"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_prdct_avrg_cycle_time"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_prdct_cnt_per_hour"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="feed_overrde"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="spd_load"></spring:message>	
							</td>
						</tr>
						<tr>
							<td class='content' id="daily_target_cycle"></td>
							<td class='content' id="complete_cycle"></td>
							<td class='content' id="prdctPerCyl"></td>
							<td class='content' id="cylCnt"></td>
							<td class='content' id="daily_avg_cycle_time"></td>
							<td class='content' id="daily_length"></td>
							<td class='content' id="feedOverride"></td>
							<td class='content' id="spdLoad"></td>
						</tr>
						<tr>
							<TD class="title_span" colspan="2">
								<spring:message code="dailystackedstatus"></spring:message>	
							</TD>
							<TD class="title_span" colspan="3" >
								<spring:message code="prdct_status"></spring:message>	
							</TD>
							<TD class="title_span" colspan="3" >
								<spring:message code="msg"></spring:message>
							</TD>
						</tr>
						<Tr>
							<td rowspan="3" colspan="2" style="text-align: center; vertical-align: top;" >
								<span id="cutTime">Cut</span> <span id="cutSpan">0</span>
								<span id="inCycleTime"><spring:message code="incycle"></spring:message></span> <span id="inCycleSpan">0</span>
								<span id="waitTime"><spring:message code="wait"></spring:message></span> <span id="waitSpan">0</span>
								<span id="alarmTime"><spring:message code="stop"></spring:message></span> <span id="alarmSpan">0</span>
								<span id="noConnTime"><spring:message code="noconnection_board"></spring:message></span> <span id="noConnSpan">0</span>
								
								<img alt="" src="${ctxPath }/images/statusChart.png" id="statusChart">
								
								<div id="opratio_gauge" style="float: left;"></div>
								<div id="cutting_gauge" style="float: right;"></div>
								<span id="opratio_span"></span>
								<span id="cuttingratio_span"></span>
								
							</td>
							<td colspan="3" style="text-align: center; vertical-align: bottom;" id="prdctChart_td">
								<!-- <span id="target_ratio" ></span>
								<div id="prdctChart" ></div> -->
								<center>
									<div id="gauge"></div>
								</center> 
							</td>
							<td colspan="3" style="background-color: rgb(25,25,25); vertical-align: top;" >
								<!-- <div id="program_div"></div> -->
								<div id="alarm_div"></div>
							</td>
						</Tr>
						<tr>
							<TD class="title_span" colspan="6">
								<spring:message code="operation_status"></spring:message>	
								
								<table id="date_table">
									<tr>
										<td>
											<button id="up">▲</button><button id="down">▼</button>
										</td>
										<td>
											<input type="date" id="today" >		
										</td>
									</tr>
								</table>
							</TD>
						</tr>
						<tr>
							<td colspan="6" style="text-align: center; vertical-align: top;" id="timeChart_td">
								<div id="timeChart"></div>
								<div id="spdFeed"></div>
								<table id="legend">
									<Tr>
										<td ><div id="spdOvrrdLine"></div></td>
										<Td>Spindle Override</Td>
										<td><div id="spdLoadLine"></div></td>
										<Td><spring:message code="spd_load"></spring:message></Td>
										
									</Tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	
